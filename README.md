<h1> Padrão de Desenvolvimento Facade </h1>

<h3>Padrão de Projeto</h3>

O conceito de Padrão de Pojeto (Design Pattern) foi criado na década de 1970 pelo arquiteto Cristopher Alexander.

Padrões de projeto fazem parte da tecnologia avançada de orientação a objetos.

Grupos de estudos sobre padrões sugerem que as pessoas aprendam padrões de projetos somente depois de terem dominado práticas básicas de orientação a objetos.

Eles são modelos de como resolver o problema do qual trata, podendo ser usado em diferentes situações.

23 destes modelos estão documentados no livro "Design Patterns: Elements of Reusable Object-Oriented Software", nos quais seus autores descrevem uma estrutura para catalogar e descrever padrões de projeto orientados a software.

Segundo Cristopher, um padrão deve possuir as seguintes características:
* Encapsulamento
* Generalidade
* Equilíbrio
* Abstração
* Abertura
* Combinatoriedade
 
Cristopher também definiu que um padrão deve possuir um formato, dividido em cinco partes:
1. Nome
2. Exemplo
3. Contexto
4. Problema
5. Solução

Os padrões, por sua ver, dividem-se em categorias:
* Padrões de criação
* Padrões de estruturação
* Padrões de comportamento
* Padrões de concorrência


<h3>O Padrão Facade</h3>

O padrão de projeto Facade é um modelo estrutural, ou seja, é um padrão que trata da associação entre classes e objetos.

Este padrão oculta toda a complexidade de uma ou mais classes através de uma Facade (Fachada) e a sua intenção é simplificar uma interface.

Com o Padrão Facade podemos simplificar a utilização de um subsistema complexo apenas implementando uma classe que fornece uma interface única e mais razoável, porém sem limitar o acesso as funcionalidades de baixo nível.

É importante ressaltar que o padrão Facade não “encapsula” as interfaces do sistema, ele apenas fornece uma interface simplificada para acessar as suas funcionalidades.

<h4> Vantagens </h4>
 * Reduz drasticamente o acoplamento entre as camadas do projeto.
 * Mantém a arquitetura coerente;
 * Disponibiliza alta manutenabilidade para o código;
 * É de fácil aplicação em projetos;
 * Simplifica e unifica uma interface grande em um conjunto complexo de interfaces.
 
<h4> Desvantagens </h4>
Não foram encontradas desvantagens neste padrão. 


<h2> Uso da Facade <h2>

<h3> Dois clientes utilizando uma única Facade </h3>

![Exemplo](fotos/facade01.jpg)


<h3>Facade acessando classes do subsistema </h3>

![](fotos/facade02.jpg)


<h3> Exemplo de cadastro de cliente sem facade </h3>

![](fotos/facade03.JPG)


<h3> Exemplo de cadastro de cliente com facade </h3>

![](fotos/facade04.JPG)



<h3>Usos do Facade no Laravel</h3>

https://laravel.com/docs/5.0/facades

<h3>Conclusões</h3>
O uso do Facade, principalmente em projetos complexos, é extremamente indicado, visto que ele visa desacoplar o código tornando-o mais fácil de ser interpretado e manutenido. 
Além disso, alguns métodos podem ser implementados dentro da própria Facade, se assim for necessário. 
Sua fácil aplicação é sua principal vantagem.



<h3>Referências</h3>

https://www.devmedia.com.br/o-padrao-facade-aplicado/12683

https://www.devmedia.com.br/padrao-de-projeto-facade-em-java/26476

https://code.tutsplus.com/pt/tutorials/design-patterns-the-facade-pattern--cms-22238

 
 

















