package com.mycompany.escola;

import java.util.Scanner;
import javax.persistence.Persistence;

/**
 *
 * @author Avell-Caetano
 */
public class FacadeGenerica {

    public void autoMatricula() {
        Aluno a = alunoCad();
        matriculaCad(a);
    }

    public Aluno alunoCad() {
        return saveAluno(fillAluno());
    }

    private Aluno fillAluno() {
        Aluno a = new Aluno();
        a.setNome(leitor("Digite nome: "));
        return a;
    }

    private Aluno saveAluno(Aluno a) {
        AlunoJpaController ajpa = new AlunoJpaController(Persistence.createEntityManagerFactory("escola"));
        ajpa.create(a);
        return a;
    }

    private void matriculaCad(Aluno a) {
        Matricula m = fillMatricula(a);
        saveMatricula(m);
    }

    private Matricula fillMatricula(Aluno a) {
        Matricula m = new Matricula();
        m.setAluno(a);
        return m;
    }

    private Matricula saveMatricula(Matricula m) {
        MatriculaJpaController mjpa = new MatriculaJpaController(Persistence.createEntityManagerFactory("escola"));
        mjpa.create(m);
        return m;
    }

    private String leitor(String mensagem) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(mensagem);
        String texto = scanner.nextLine();
        return texto;
    }

}
