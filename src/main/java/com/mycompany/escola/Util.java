/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.escola;

import java.util.Scanner;

/**
 *
 * @author Avell-Caetano
 */
public class Util {

    public String leitor(String mensagem) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(mensagem);
        String texto = scanner.nextLine();
        return texto;
    }

    public void menu() {
        System.out.println("--------------------------------------------------------------------");
        System.out.println("-----                       Faculade Senac                     -----");
        System.out.println("____________________________________________________________________");
        System.out.println("______________________   Opções de Cadastro  _______________________");
        System.out.println("--------------------------------------------------------------------");
        System.out.println("---    1 - Cadastro Aluno                                        ---");
        System.out.println("---    2 - Cadastro de aluno c/ matricula                        ---");
        System.out.println("--------------------------------------------------------------------");
        String opcao = leitor("Digite a opção: ");

        switch (opcao) {
            case "1":
                new FacadeGenerica().alunoCad();
                menu();
                break;
            case "2":
                new FacadeGenerica().autoMatricula();
                menu();
                break;
            default:
                System.out.println("Opção Inválida");
                menu();
                break;
        }
    }

}
